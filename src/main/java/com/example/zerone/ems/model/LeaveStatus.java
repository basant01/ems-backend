package com.example.zerone.ems.model;

public enum LeaveStatus {

	
	 PENDING, 
	 
	 APPROVED, 
	 
	 REJECTED;
	
}
/*
{
	"doj":"11-10-98",
	"empName":"rohan",
	"empEmail":"ros@gmail.com",
	"line_leaves":[
		{
			"leaveReason":"nothing",
			"comments":"no",
			"startDate":"11-10-98",
			"endDate":"21-11-98",
			"leaveStatus":"PENDING"
		}
		
		
		]
}
*/